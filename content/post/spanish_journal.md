{
 "disqus_url" : "http://conr.ca/post/spanish_journal/",
 "disqus_title" : "Spanish Journal",
 "Title": "Spanish Journal",
 "date": "2010-08-24",
 "Keywords": [],
 "Tags": [],
 "Slug": "spanish_journal",
 "Section": "post"
}
This might not be very interesting to most people, but as part of my requirements for a correspondence Spanish course I will be taking, my professor has charged me with keeping a journal in Spanish about communication.&nbsp; In order to make it easy, I decided to make it a blog.&nbsp; So, if you can read Spanish look here: <a href="http://angloperu.blogspot.com/">http://angloperu.blogspot.com/</a>.&nbsp; Feel free to comment if you want to correct my grammar or anything.&nbsp;<br />Until next time.
