{
 "disqus_url" : "http://conr.ca/post/i_am_not_mia/",
 "disqus_title" : "I am not MIA",
 "Title": "I am not MIA",
 "date": "2011-04-12",
 "Keywords": [],
 "Tags": [],
 "Slug": "i_am_not_mia",
 "Section": "post"
}
I know I've been MIA for a little while now. Taking upwards of a week to reply to birthday emails. I still have messages from the start of March to answer (sorry, Kris). But I am not making excues. I've been busy lately. Work has picked up in pace, which is great. I've somehow been made to make radio and TV commercials - you know, beacuse that's my area of expertise. <br /><br />Anyway, outside of work I've been writing political letters (which you should sign):<br /><br /><div align=center><script type="text/JavaScript">var Care2P_Parameters=["http://www.thepetitionsite.com/xml/petitions/998/490/628/feed.rss", "small", "single", "1007", "0", "#7cbb42", "#eb6924", "#c739e"];</script><script type="text/JavaScript" src="http://dingo.care2.com/petitions/widget/common/petition_embed_br.js"></script></div><br />I've also been busy being a movie star/writer/director. This project took a lot of time, but my Spanish Prof tells me she loves it! That is great news for me. If you read this blog but haven't bothered watching the film yet ... you're a jerk.<br /><br /><div align=center><object width="416" height="337"><param name="movie" value="http://www.youtube.com/cp/vjVQa1PpcFMDs1oPUeLIxsN4ZUrTjKybL2aiZq3l1PU="></param><embed src="http://www.youtube.com/cp/vjVQa1PpcFMDs1oPUeLIxsN4ZUrTjKybL2aiZq3l1PU=" type="application/x-shockwave-flash" width="416" height="337"></embed></object></div><br />Other than these two projects, I haven't been up to too much. I signed up for a GIS course which will start on Saturday. It is a little pricey but I am really interested in it so I guess it's worth about forty bucks a month.<br /><br />Alex and I bought a skateboard yesterday... so maybe I'll learn how to use it. Not much else is new. Until next time! Sorry this post is so quick - my work computer sucks.
