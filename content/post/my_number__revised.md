{
 "disqus_url" : "http://conr.ca/post/my_number__revised/",
 "disqus_title" : "My number ... revised.",
 "Title": "My number ... revised.",
 "date": "2010-09-24",
 "Keywords": [],
 "Tags": [],
 "Slug": "my_number__revised",
 "Section": "post"
}
Hello, so I've determined that you do not need a city code to call internationally to Peru.  Thus, my number (to call or text) should be: +51952724844<br /><br />I'll write a real post over the weekend.<br /><br />EDIT: Okay, so I finally figured out what I had wrong. Anatomy of Conor's phone number:<br /><br />+ - international dialling code (00 in most of the world, 011 in Canada and the US)<br />51 - Peru's country code<br />9 - indicating a mobile phone<br />52 - Tacna's city code<br />714844 - My six-digit phone number
