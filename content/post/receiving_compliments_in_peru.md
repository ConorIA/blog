{
 "disqus_url" : "http://conr.ca/post/receiving_compliments_in_peru/",
 "disqus_title" : "Receiving Compliments in Peru",
 "Title": "Receiving Compliments in Peru",
 "date": "2010-12-13",
 "Keywords": [],
 "Tags": [],
 "Slug": "receiving_compliments_in_peru",
 "Section": "post"
}
This quick entry is a response to a <a href="http://beckyinfotobi.blogspot.com/2010/12/receiving-compliments-in-ghana.html">lovely little entry</a> by Becky regarding the compliments she receives in Ghana. I decided that since I am also well praised here, I would share a few quick compliments that my friends have given me over the last week:<br /><br />Example number one: My haircut.<br /><br /><ul><li>Last week I got my hair cut. It is getting hot here so I went with the standard buzz cut, nice and short and uniform. My friends very very impressed with my haircut, making comments such as:</li><ul><li>Oscar to his baby: 'Look Mahdia, your uncle Conor is a neo-Nazi now.'</li><li>Marleni: 'I think the clippers were broken.'</li><li>Carmen: 'What happened to your hair?'</li></ul><li>Oscar also referred to me as Herr Conor for the weekend.</li></ul>Example number two: My incredible command of the Spanish language:<br /><br /><ul><li>My Spanish-speaking friends continue to be amazed at my near-native accent.&nbsp;</li><ul><li>Gabby: 'Conor puts his accents where he wants to.'</li><li>Debora: 'In English there are no accent markers to show emphasis. In Spanish they are written out. How do you miss them?'&nbsp;</li></ul></ul><div>I don't want to seem&nbsp;conceited, so I won't share any more compliments. Living away from home in a foreign country can be tough, but as you can see from those above my friends in Peru are absolutely delightful and do a wonderful job of helping me to maintain my self-esteem and motivation while I'm here. I don't know what I'd do without them!</div>
