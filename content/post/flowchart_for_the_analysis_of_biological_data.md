{
 "disqus_url" : "http://conr.ca/post/flowchart_for_the_analysis_of_biological_data/",
 "disqus_title" : "Flowchart for the analysis of biological data",
 "Title": "Flowchart for the analysis of biological data",
 "date": "2012-02-16",
 "Keywords": ["academia", "biology", "statistics", "creative commons", "open access", "university", "resource", "school"],
 "Tags": ["academia", "biology", "statistics", "creative commons", "open access", "university", "resource", "school"],
 "Slug": "flowchart_for_the_analysis_of_biological_data",
 "Section": "post"
}
I had an exam in my biostats course today. In preparing for the exam, I came up with a useful flowchart helping to decide what statistical tests to use with what statistical data. If you are ever asking yourself this question, maybe it will help you too! <a href="http://creativecommons.org/licenses/by-sa/3.0/" target="_blank">CC BY-SA 3.0</a> license, folks. <br /><br /><div class="separator" style="clear: both; text-align: center;"><a href="http://2.bp.blogspot.com/-_ML7AeXqWEQ/Tz2dKyAto2I/AAAAAAAACiQ/7CyU2Ev--E4/s1600/biostatsflowchart.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="308" src="http://2.bp.blogspot.com/-_ML7AeXqWEQ/Tz2dKyAto2I/AAAAAAAACiQ/7CyU2Ev--E4/s400/biostatsflowchart.png" width="400" /></a></div><br />Get the flowchart as a <a href="https://docs.google.com/open?id=0B1Ro8tWtldk-MWYzMDNjMDYtN2IwYS00ZGQyLTllYTEtZTgwNmY5ZTA4YWQx" target="_blank">pdf</a>, a <a href="https://docs.google.com/open?id=0B1Ro8tWtldk-NjA0ZDNmOGItMmQzYS00ZTQyLWIwNDktYmViYzQyYmEwNmRi" target="_blank">png</a>, or an <a href="https://docs.google.com/open?id=0B1Ro8tWtldk-MDcxZDJjODktMjljMS00YjA4LWJiYjUtNTEyNTFmMWUwMDgy" target="_blank">odg</a>.
