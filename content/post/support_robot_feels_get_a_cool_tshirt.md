{
 "disqus_url" : "http://conr.ca/post/support_robot_feels_get_a_cool_tshirt/",
 "disqus_title" : "Support Robot Feels! Get a cool t-shirt!",
 "Title": "Support Robot Feels! Get a cool t-shirt!",
 "date": "2012-02-09",
 "Keywords": ["music", "lo-fi", "youtube", "bandcamp", "ambient", "robot feels", "support"],
 "Tags": ["music", "lo-fi", "youtube", "bandcamp", "ambient", "robot feels", "support"],
 "Slug": "support_robot_feels_get_a_cool_tshirt",
 "Section": "post"
}
As you may have realized, I really support my little brother in his musical endeavours. As such, I designed him a little t-shirt, drawing a robot by hand and spending WAY TOO LONG digitizing it. The result is pretty cute. To make it even better, $5 from each T-shirt purchase will go directly to him towards his Robot Feels project.<br /><br /><div style="text-align: center;"><a href="http://j.mp/robotwearstshirts"><img alt="Men's Standard Weight T-Shirt - T-Shirts Robot Feels Shirt" src="http://image.spreadshirt.com/image-server/image/product/19713434/view/1/type/png/width/250/height/250" style="border: 0px none; width: 190px;" title="Men's Standard Weight T-Shirt - T-Shirts Robot Feels Shirt" /></a></div>For more on Robot Feels, visit <a href="http://robotfeels.com/">his website</a>. Support him by buying his E.P., which will cost you about the same as a cup of coffee.
