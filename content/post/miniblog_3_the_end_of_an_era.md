{
 "disqus_url" : "http://conr.ca/post/miniblog_3_the_end_of_an_era/",
 "disqus_title" : "Miniblog 3: The end of an era",
 "Title": "Miniblog 3: The end of an era",
 "date": "2012-10-08",
 "Keywords": ["friends", "goodbyes", "peru", "tarapoto", "departures"],
 "Tags": ["friends", "goodbyes", "peru", "tarapoto", "departures"],
 "Slug": "miniblog_3_the_end_of_an_era",
 "Section": "post"
}
<div>For the third day in a row, my two bros, Rony and Fernando and I, are sat at <a href="http://www.facebook.com/heladeria.anonas.9" target="_blank"><i>Anonas Heladeria</i></a> (shout-out to our favourite place in Tarapoto), working away on our respective <i>tareas</i>. While it may be the umpteenth time that we've done this, it very well may be one of the last. Rony, our dear friend and colleague has only one week left with us before he jet-sets back to Canada next Monday. The three of us had that sort of friendship where, having known each other for a few short weeks, it feels like we've known each other for years. I attribute no small part of my contentment and comfort in Tarapoto to having friends like these guys. Rony has been a great friend and Fernando and I will cry for days over his departure... oh, did I say "cry for days"? I meant be totally manly and tough about things. Bye Ronycito, we'll miss you mountains, bro. I have no doubt we'll be in touch! &nbsp;</div><div><br /></div><table align="center" cellpadding="0" cellspacing="0" class="tr-caption-container" style="margin-left: auto; margin-right: auto; text-align: center;"><tbody><tr><td style="text-align: center;"><a href="http://4.bp.blogspot.com/-DlkMqWIYb7g/UHNdGeJ_2qI/AAAAAAAAD04/uh7ZZyRYKr0/s1600/IMG_20121008_174941-picsay.jpg" imageanchor="1" style="margin-left: auto; margin-right: auto;"><img border="0" height="240" src="http://4.bp.blogspot.com/-DlkMqWIYb7g/UHNdGeJ_2qI/AAAAAAAAD04/uh7ZZyRYKr0/s320/IMG_20121008_174941-picsay.jpg" width="320" /></a></td></tr><tr><td class="tr-caption" style="text-align: center;">We're cute.</td></tr></tbody></table><div><br /></div><div>P.S. A warm welcome to Fred, a new volunteer from Quebec who made sure to cameo for the photo!</div>
