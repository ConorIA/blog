{
 "disqus_url" : "http://conr.ca/post/sights_of_tacna/",
 "disqus_title" : "Sights of Tacna",
 "Title": "Sights of Tacna",
 "date": "2011-02-05",
 "Keywords": [],
 "Tags": [],
 "Slug": "sights_of_tacna",
 "Section": "post"
}
So I figure after a little more than 5 months, my general public (does that sound big-headed?) might like to know where I live. Thus I shall snap photos on my cell phone and post them here at the occasional interval. For real-time updates you can grab the <a href="https://picasaweb.google.com/data/feed/base/user/conor.anderson/albumid/5570338928811908945?alt=rss&amp;kind=photo">RSS here</a> or check on my <a href="http://www.google.com/profiles/conor.anderson">Buzz here</a>. I might even try to GPS tag some of the fotos.<br /><br /><div style="text-align: center;"> <embed flashvars="host=picasaweb.google.com&amp;captions=1&amp;noautoplay=1&amp;hl=en_US&amp;feat=flashalbum&amp;RGB=0x000000&amp;feed=https%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2Fconor.anderson%2Falbumid%2F5570338928811908945%3Falt%3Drss%26kind%3Dphoto%26hl%3Den_US" height="267" pluginspage="http://www.macromedia.com/go/getflashplayer" src="https://picasaweb.google.com/s/c/bin/slideshow.swf" type="application/x-shockwave-flash" width="400"></embed></div>
