{
 "disqus_url" : "http://conr.ca/post/the_post_that_never_was/",
 "disqus_title" : "The Post that Never Was",
 "Title": "The Post that Never Was",
 "date": "2011-06-20",
 "Keywords": ["work", "jungle", "tarapoto", "uniterra", "san martín"],
 "Tags": ["work", "jungle", "tarapoto", "uniterra", "san martín"],
 "Slug": "the_post_that_never_was",
 "Section": "post"
}
Well. My last update was going to start like this:<br /><blockquote>I am currently sitting in a cafe in the airport in Tarapoto, awaiting my flight that is due to take off in an hour and a half. It's been a very short four days here and I had a blast.<br /><br />Working with Jairo is always a laugh. He was my first 'official friend' in Peru, and he makes sure I remember the time in Lima when we went out and drank 'chopps'.<br /><br />During my time in Tarapoto, I was teased by the fact that I had NO time to explore what the city had to offer. Our very limited trekking was great though.<br /><br />There isn't much I can say about Tarapoto that isn't good. I only stayed a short time, so I am still in my honeymoon period with the city as I write. The views are amazing - from the green landscape to the ... landscapes with legs? ... - all of it is beautiful.<br /><br />My laptop battery is dying so I'm going to have to leave it here and pick up from Lima.</blockquote>Only thing is, I never picked it up in Lima. That is where my Tarapoto blog ends, so I'm going to try and make up for it by posting a few pictures so you can see what I mean. Unfortunately, I don't have many pictures and I don't have any of the landscapes with legs (but they were beautiful). <br /><br /><div style="text-align: center;"><embed type="application/x-shockwave-flash" src="https://picasaweb.google.com/s/c/bin/slideshow.swf" width="400" height="267" flashvars="host=picasaweb.google.com&captions=1&hl=en_US&feat=flashalbum&RGB=0x000000&feed=https%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2Fconor.anderson%2Falbumid%2F5620312077218864561%3Falt%3Drss%26kind%3Dphoto%26authkey%3DGv1sRgCMynq4-FmYH0Bg%26hl%3Den_US" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed><br /></div>
