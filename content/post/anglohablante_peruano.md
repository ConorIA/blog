{
 "disqus_url" : "http://conr.ca/post/anglohablante_peruano/",
 "disqus_title" : "Anglohablante Peruano",
 "Title": "Anglohablante Peruano",
 "date": "2011-03-13",
 "Keywords": [],
 "Tags": [],
 "Slug": "anglohablante_peruano",
 "Section": "post"
}
For those of you who have been curious what I've been up to this past week: <br /><br />I've been nice and busy tweaking the script of my shortfilm for Spanish class, and going to dance lessons. I already posted a video of my awesome dancing skills (see the last post). This weekend we've been busy filming for the shortfilm. Here is a sneak-peak at some goofing around while preparing for the last scene. <br /><br /><iframe title="YouTube video player" width="640" height="390" src="http://www.youtube.com/embed/Yt-M-fMLkxo?rel=0" frameborder="0" allowfullscreen></iframe>
