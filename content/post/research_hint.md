{
 "disqus_url" : "http://conr.ca/post/research_hint/",
 "disqus_title" : "Research Hint",
 "Title": "Research Hint",
 "date": "2010-12-10",
 "Keywords": ["academia", "university", "research", "resource", "school"],
 "Tags": ["academia", "university", "research", "resource", "school"],
 "Slug": "research_hint",
 "Section": "post"
}
It's been a while since I've sent any of my little tips along to my peers, so this post isn't for the family.<br /><br />Today I noticed a <i>very </i>useful feature on Google. If you go to the Google.ca homepage, you will notice a small box that says 'Language Tools'. This serves to translate your search into another language and then return all of the foreign-language results back in English. The&nbsp;translations&nbsp;read like translations, so I figure I'll keep looking in Spanish, but for those of you in the east, this could be a very useful tool to get some country-specific examples.<br /><br />In fact, I took this little screen capture to show what the results would look like if we were all (more or less) researching my topic. There are a lot of hits. Unfortunately for Adam there is no Tamil option, but there is Hindi which, (forgive me if I'm wrong) I figure would have much more publications anyway.<br /><br /><table align="center" cellpadding="0" cellspacing="0" class="tr-caption-container" style="margin-left: auto; margin-right: auto; text-align: center;"><tbody><tr><td style="text-align: center;"><a href="http://4.bp.blogspot.com/_ARe3vRR4TFQ/TQJv7h5KQbI/AAAAAAAABI0/5JzBggE1L_A/s1600/research.jpg" imageanchor="1" style="margin-left: auto; margin-right: auto;"><img border="0" height="480" src="http://4.bp.blogspot.com/_ARe3vRR4TFQ/TQJv7h5KQbI/AAAAAAAABI0/5JzBggE1L_A/s640/research.jpg" width="640" /></a></td></tr><tr><td class="tr-caption" style="text-align: center;">Notice results for each language.</td></tr></tbody></table><br /><span id="goog_1084055097"></span><span id="goog_1084055098"></span><br />Enjoy and happy searching ... but now, it's lunch time!
