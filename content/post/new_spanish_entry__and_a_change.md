{
 "disqus_url" : "http://conr.ca/post/new_spanish_entry__and_a_change/",
 "disqus_title" : "New Spanish Entry ... and a change.",
 "Title": "New Spanish Entry ... and a change.",
 "date": "2010-10-14",
 "Keywords": [],
 "Tags": [],
 "Slug": "new_spanish_entry__and_a_change",
 "Section": "post"
}
I´ve written another entry in my <a href="http://angloperu.blogspot.com/">Spanish journal</a>. I've also decided to close the (Spanish) blog, to keep it personal in the event that my frustrations come across as criticisms. If you'd like to read it, just comment on this post, Facebook me, email me or something.
