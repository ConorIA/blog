{
 "disqus_url" : "http://conr.ca/post/happy_movember/",
 "disqus_title" : "Happy Movember!",
 "Title": "Happy Movember!",
 "date": "2010-11-01",
 "Keywords": [],
 "Tags": [],
 "Slug": "happy_movember",
 "Section": "post"
}
Today, Movember 1st, marks the start of my mustache growth to change  the face of men's health and raise money against prostate cancer. Enjoy  the video below.<br /><div style="text-align: center;"><object height="385" width="480"><param name="movie" value="http://www.youtube.com/v/te0hvgGVZ6w?fs=1&amp;hl=en_US&amp;color1=0x006699&amp;color2=0x54abd6"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/te0hvgGVZ6w?fs=1&amp;hl=en_US&amp;color1=0x006699&amp;color2=0x54abd6" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object></div>Links:<br /><ul><li><span style="font-size: small;"><b><a href="https://www.movember.com/ca/donate/your-details/member_id/604932/">DONATE, DONATE, DONATE, DONATE, DONATE</a></b></span></li><li><a href="http://www.movember.com/">Movember</a></li><li><a href="http://ca.movember.com/mospace/604932/">Conor's Mo Space</a></li></ul><br /><br />I will also be keeping an up-to-date photo-a-day slideshow here:<br /><br /><div style="text-align: center;"><embed flashvars="host=picasaweb.google.com&amp;captions=1&amp;hl=en_US&amp;feat=flashalbum&amp;RGB=0x000000&amp;feed=http%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2Fconor.anderson%2Falbumid%2F5534598694981440161%3Falt%3Drss%26kind%3Dphoto%26authkey%3DGv1sRgCIyvmsGZjv783gE%26hl%3Den_US" height="267" pluginspage="http://www.macromedia.com/go/getflashplayer" src="http://picasaweb.google.com/s/c/bin/slideshow.swf" type="application/x-shockwave-flash" width="400"></embed></div>
