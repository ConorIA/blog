{
 "disqus_url" : "http://conr.ca/post/post_is_mostly_unrelated/",
 "disqus_title" : "Post is mostly unrelated",
 "Title": "Post is mostly unrelated",
 "date": "2010-07-18",
 "Keywords": [],
 "Tags": [],
 "Slug": "post_is_mostly_unrelated",
 "Section": "post"
}
Just wanted to send a quick apology down the RSS tubes to the 8 subscribers who are using the RSS feed that I put together.&nbsp; I found out that one can bundle a bunch of items through Google Reader rather than deal with Yahoo pipes and it's confusion.&nbsp; As such, you will all see a bunch of unread items and it seems that Raly's and Mary's posts are out of order, but those should all even out once you mark them all as read.&nbsp; I just feel bad if anyone tries to catch up.&nbsp;<br /><br />Until next time.
