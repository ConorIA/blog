{
 "disqus_url" : "http://conr.ca/post/dictionarycom_wotd_rap/",
 "disqus_title" : "Dictionary.com WotD Rap",
 "Title": "Dictionary.com WotD Rap",
 "date": "2012-02-02",
 "Keywords": ["rap", "youtube", "#wotd", "conorkickass", "word of the day", "Dictionary.com", "hip-hop"],
 "Tags": ["rap", "youtube", "#wotd", "conorkickass", "word of the day", "Dictionary.com", "hip-hop"],
 "Slug": "dictionarycom_wotd_rap",
 "Section": "post"
}
I made an awesome YouTube video, rapping all of the Dictionary.com words of the day for January.  I'd embed it here, but I'd rather you see it in all its glory and have those nice little 'share' buttons handy at the bottom of the screen. http://www.youtube.com/watch?v=BcVMTxFI04M
