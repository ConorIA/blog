{
 "disqus_url" : "http://conr.ca/post/search_story_fun/",
 "disqus_title" : "Search Story Fun",
 "Title": "Search Story Fun",
 "date": "2010-04-17",
 "Keywords": [],
 "Tags": [],
 "Slug": "search_story_fun",
 "Section": "post"
}
If anyone knows me, they will know I'm a Google fanboy.&nbsp; I pretty much live my life according to Google's rule.&nbsp; If I ever question anything I 'Google it', I use an Android cell phone, heck I pretty much use Google to tell me what mood I'm in on a particular day ... well, that is a bit of an exaggeration. <br /><br />Anyway, today I saw my little brother's video on Youtube that he called 'Google it'.&nbsp; (<a href="http://www.youtube.com/user/FavouriteLooseCannon">Subscribe to  him!</a>)&nbsp; They are super easy so I decided to make my own about how the placement process has worked for me so far.&nbsp; Enjoy:<br /><br /><object height="344" width="425"><param name='movie' value='http://www.youtube.com/v/UAkCvodt-Qo&hl=en_US&fs=1&'></param><param name='allowFullScreen' value='true'></param><param name='allowscriptaccess' value='always'></param><embed src='http://www.youtube.com/v/UAkCvodt-Qo&hl=en_US&fs=1&' type='application/x-shockwave-flash' allowscriptaccess='always' allowfullscreen='true' width='425' height='344'></embed></object><br /><br />P.S. <a href="http://www.youtube.com/searchstories">Make your own here</a>!
