{
 "disqus_url" : "http://conr.ca/post/5_quick_lessons/",
 "disqus_title" : "5 quick lessons",
 "Title": "5 quick lessons",
 "date": "2010-09-25",
 "Keywords": [],
 "Tags": [],
 "Slug": "5_quick_lessons",
 "Section": "post"
}
Lesson 1) In Peru, the word 'ambiental' means one of two things.  i) The environment, as in Medio Ambiente (Environmental Studies), ii) homosexual<br />Result 1) Trying to explain the kind of music I listen to (e.g. Lights out Asia or Hammock) will have people questioning my orientation.<br /><br />Lesson 2) Always wash clothes in the morning.  Don't wash clothes as the sun is going down.<br />Result 2) Clothes do not dry overnight and you end up smelling worse than you did when all of your clothes were dirty.<br /><br />Lesson 3) If you hear a siren, it is probably not the police, it is probably just a suped up taxi that is honking at you.  (This also applies to "wooga", "dododido" and other such sounds)<br />Result 3) MOVE!  You are going to be flattened.  In fact, it is often best to use a Peruvian as a guide when crossing the street.  They live in some strange equilibrium with taxi drivers.  Tourists, on the other hand, live at odds with taxi drivers.<br /><br />Lesson 4) If somebody makes a social commitment to you, expect that they will forget about it.<br />Resut 4) You either sit up waiting for a call that never comes, OR you go to the café to meet your friend, chat with the owner a bit, sit there awkwardly for a bit, have a drink, then go home.<br /><br /><br />Lesson 5) A vicious cycle.<br />BONUS: POKEMON style<br />> A dirty floor appears.<br />Conor > Go Broom!<br />- Broom uses  sweep! <br />> It is not very effective.<br />Conor > Broom, come back!  Go mop!<br />- Mop uses clean!<br />> It is super effective!<br />> Dirty floor is defeated.  <br />---<br />> A wet floor appears!<br />Conor > Go shoes!<br />> Wetfloor evolves into dirty floor!<br />Conor > Go broom! ... etc
