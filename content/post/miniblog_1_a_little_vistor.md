{
 "disqus_url" : "http://conr.ca/post/miniblog_1_a_little_vistor/",
 "disqus_title" : "Miniblog 1: A little vistor",
 "Title": "Miniblog 1: A little vistor",
 "date": "2012-08-27",
 "Keywords": ["funny", "peru", "cute", "tarapoto", "life", "miniblog"],
 "Tags": ["funny", "peru", "cute", "tarapoto", "life", "miniblog"],
 "Slug": "miniblog_1_a_little_vistor",
 "Section": "post"
}
Tonight, after work, I was in my room watching Breaking Bad. I leave the door open as there is often a nice breeze that blows down between the two cordilleras on wither side of Tarapoto. Suddenly, a little girl - the owner's niece, I assumed - walked straight into my room. She <strike>couldn't have been more than 7 or 8</strike> was four years old*  and was totally adorable. She asked me a hundred questions, inquisitively picked things up off the floor, examining them, and she inspected everything on my desk. I answered all her questions and the landlord eventually came to my room. Rather than calling the little girl away as I though she might, she said, "Conor, who's little girl is that?" I didn't know. Neither did she. Turns out she had left the front door open and this little girl walked straight in, through to the back of the house and in through my open door. We asked the little girl who she was, but she insisted (dishonestly) that she didn't have a name, she didn't live anywhere, she didn't have a mom. When the owner went out to look for her mother the little girl ran to me and held onto my legs so that I wouldn't kick her out. Eventually the landlord came and fighting tiny fists, returned the little wanderer to her mother. This will certainly stick with me as being one of the strangest, but cutest, things I've ever witnessed!<br /><br /><span style="font-size: x-small;">* I told my neighbour this story and he said the girl is only 4! I'm shocked, she was so intelligent. </span>
