{
 "disqus_url" : "http://conr.ca/post/august/",
 "disqus_title" : "August",
 "Title": "August",
 "date": "2011-02-20",
 "Keywords": ["friends", "work", "peru", "august", "coming home", "extension", "placement", "mandate", "bored"],
 "Tags": ["friends", "work", "peru", "august", "coming home", "extension", "placement", "mandate", "bored"],
 "Slug": "august",
 "Section": "post"
}
Last week we went to Lima. It was time for another Sectoral Meeting to review the accomplishments over the last fiscal year (April to March) and look towards the future.<br /><br />While sitting in the airport in Tacna on Sunday morning - me a little hungover from the might before - we started talking about the winter (your summer, Canada). Mahdia's madate is over by the end of April. Marleni is leaving in June until the end of July on maternity leave. My madate expires on the ninth of June. Pedro would, therefore, be in charge of the Sanitation Educaction Program, take on a lot of Marleni's work and have his own stuff to do.<br /><br />I sat there for a while thinking. I like Tacna, I really do. Sometimes I forget that though. Some days this little desert community loses it's charm and the desert wasteland that it is begins to seep through the cracks in the pavement, in through the windows and coats everything in a small layer of I-hate-this-place dust. Sometimes though, the city manages to keep its facade up, gently supported by my wonderful friends on lazy weekends or tired weeknights.<br /><br />"I can stay until August." "I can help out until the middle of August." On Friday we sent the letter requesting WUSC's approval of a two-month extension to my mandate. They've given it the OK. I'll update my blog when Ottawa sends me a new departure date.<br /><br />I'm actually pretty excite because Pedro and I work really well together. It's easier when it is the two of us as we understand one another. It will be nice to be on my own for a while as I'll have more responsability. Also, I'll have more time to work on my thesis, which is a definite YES. <br /><br />See you in the fall.
