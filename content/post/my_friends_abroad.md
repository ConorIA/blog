{
 "disqus_url" : "http://conr.ca/post/my_friends_abroad/",
 "disqus_title" : "My friends abroad",
 "Title": "My friends abroad",
 "date": "2010-09-30",
 "Keywords": [],
 "Tags": [],
 "Slug": "my_friends_abroad",
 "Section": "post"
}
Sometimes, when I explain to people here that there are 17 of us in the program, they think it is incredible.  They always ask where everyone else is. I've updated the map with the latest information. If anyone in the class wants to update themselves, I'm pretty sure everyone in the Google Group has editing privilidges.  <br /><br /><div style="text-align: center;"><iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;msa=0&amp;msid=114190655101365474335.00048488efd5484a2db3e&amp;ll=9.102097,9.84375&amp;spn=127.764478,225&amp;z=2&amp;output=embed"></iframe><br /><small>View <a href="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;msa=0&amp;msid=114190655101365474335.00048488efd5484a2db3e&amp;ll=9.102097,9.84375&amp;spn=127.764478,225&amp;z=2&amp;source=embed" style="color:#0000FF;text-align:left">IDS Placements 2010-2011</a> in a larger map</small><br /></div>
