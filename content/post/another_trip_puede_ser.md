{
 "disqus_url" : "http://conr.ca/post/another_trip_puede_ser/",
 "disqus_title" : "Another trip? ¡Puede ser!",
 "Title": "Another trip? ¡Puede ser!",
 "date": "2012-02-24",
 "Keywords": ["2012", "peru", "tarapoto", "placement", "uniterra"],
 "Tags": ["2012", "peru", "tarapoto", "placement", "uniterra"],
 "Slug": "another_trip_puede_ser",
 "Section": "post"
}
<div><p>I received word this week that I am scheduled to return to Peru on the 1st of August. However, much like when I left in 2010, I refuse to call anything final until I have an e-ticket in my inbox, but I'm already pretty excited. This time, I should be in Tarapoto, in the northeast of Peru (I'll add a map to this post later if I remember - I'm using my tablet right now). </p><p>Tarapoto presents a really great opportunity for me, as, unlike Tacna, it isn't a desert where the main issue is water quantity. Instead, Tarapoto's education program is focused on issues that affect water quality like deforestation and pesticide use. Tarapoto is 'selva alta', a sort of higher-altitude rainforest, so it is a really neat climate zone and a utopia for an ecologist (or should I say environmental biologist) like myself. To be perfectly honest, I have yet to see the project I'll be working on, but I am already delighted to have an opportunity to use some of my academic formation in conservation and biodiversity protection. I'll update the ol' plog, now nicely located at conr.ca (note the missing 'o'), as I progress through the application process. </p><p>So Canadian friends, I've been a hermit for the last few weeks as I've been working on my thesis, but I promise to see more of you in the coming weeks, but especially after April (my last exam is on April 30 - seems UofT just doesn't want to let me go!)</p><p>Also, if, by chance, you're stumbling on my blog on a search for your next travel destination, I'll update my Couchsurfing.org profile once I've got a place in Peru. </p><p>Thanks for reading.</p></div>
