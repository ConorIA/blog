{
 "disqus_url" : "http://conr.ca/post/support_open_souce_software_and_conors_geekiness/",
 "disqus_title" : "Support open souce software (and Conor's geekiness)",
 "Title": "Support open souce software (and Conor's geekiness)",
 "date": "2010-09-30",
 "Keywords": [],
 "Tags": [],
 "Slug": "support_open_souce_software_and_conors_geekiness",
 "Section": "post"
}
For those of you who know me well enough, you'll know I'm a huge geek.  <br />What's more is that I'm a massive geek about open source software.  <br /><br />I used to use OpenOffice.org exclusively.  Recently, however, Oracle's takeover of Sun has seen Sun's open source division go down the tubes.  In response to this, a large subset of the developers of OpenOffice.org hace forked the code over to a new project called LibreOffice.<br /><br />"Okay, Conor. You've proved your point. You're a geek. What do you want from us?"<br /><br />Well, that is simple.  Or, as they say in French: simple.  (Or Spanish: simple).  <br />All you have to do is sign the below petition to have Oracle donate the OpenOffice.org trademark to the current Document Foundation (what a terrible name).  That's all from me for today. <br /><br /><div style="text-align: center;"><script src="http://www.petitionspot.com/fwidget.js?petition=documentfoundation&amp;width=280&amp;height=250" type="text/javascript"></script><noscript>Please sign the petition Create an independent OpenOffice.org-Foundation by clicking &amp;lt;a href="http://www.petitionspot.com/petitions/documentfoundation"&amp;gt;here&amp;lt;/a&amp;gt; - &amp;lt;a href="http://www.petitionspot.com/"&amp;gt;Start a Petition&amp;lt;/a&amp;gt; or &amp;lt;a href="http://www.petitionspot.com/"&amp;gt;online petition&amp;lt;/a&amp;gt;</noscript><br /></div><br />P.S. I am still alive and well and (hardly) working in Peru.
