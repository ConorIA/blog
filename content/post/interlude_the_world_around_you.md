{
 "disqus_url" : "http://conr.ca/post/interlude_the_world_around_you/",
 "disqus_title" : "Interlude: The World Around You",
 "Title": "Interlude: The World Around You",
 "date": "2012-01-27",
 "Keywords": ["music", "lo-fi", "youtube", "bandcamp", "ambient", "robot feels", "support"],
 "Tags": ["music", "lo-fi", "youtube", "bandcamp", "ambient", "robot feels", "support"],
 "Slug": "interlude_the_world_around_you",
 "Section": "post"
}
My youngest brother has put together his first full-length EP on his bandcamp! It is really cool, alternative stuff. You should most certainly check it out!<br /><br /><div align="center"><iframe allowtransparency="true" frameborder="0" height="100" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=1158012292/size=venti/bgcol=FFFFFF/linkcol=4285BB/" style="display: block; height: 100px; position: relative; width: 400px;" width="400">&amp;amp;lt;p&amp;amp;gt;&amp;amp;amp;lt;a href="http://robotfeels.bandcamp.com/album/the-world-around-you"&amp;amp;amp;gt;The World Around You by Robot Feels&amp;amp;amp;lt;/a&amp;amp;amp;gt;&amp;amp;lt;/p&amp;amp;gt;</iframe></div>
