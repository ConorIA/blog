{
 "disqus_url" : "http://conr.ca/post/one_of_the_hardest_things_ever/",
 "disqus_title" : "One of the hardest things ever",
 "Title": "One of the hardest things ever",
 "date": "2011-08-13",
 "Keywords": [],
 "Tags": [],
 "Slug": "one_of_the_hardest_things_ever",
 "Section": "post"
}
Today was the day that I had to say goodbye to the wonderful friends I've had the pleasure of making during the last year. The six most important Tacneños to me all came by my house this morning to see me off. Oscar and his wonderful daughther were the first to show up and keep my company as I packed my life into two bags. Kathy came by shortly after. When Libertad arrived we took some photos and waited for Viviana and Oscar to arrive. <br /><br /><div class="separator" style="clear: both; text-align: center;"><a href="http://2.bp.blogspot.com/-VyFS8JneP34/TkdM8UUe_jI/AAAAAAAAB6Q/T8BLBZbKmnk/s1600/100_1708.JPG" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="http://2.bp.blogspot.com/-VyFS8JneP34/TkdM8UUe_jI/AAAAAAAAB6Q/T8BLBZbKmnk/s640/100_1708.JPG" width="100%" /></a></div><br /><br />Going to the airport was different this time. We were laughing as usual, but the tension was thick. Waiting for the airplane was even harder. Standing around just waiting to say goodbye wasn't fun. Every time I looked at Kathy and the way her eyes refused to leave the ground, I could feel my own start to fog up. <br /><br />It was too hard to leave behind such wonderful people. The only consolation today was that I wasn't really saying 'goodbye', I was saying 'see you later'. <br /><br />It's been a wild ride this past year, and I've had various friendships that drifted apart o faded away, but I'm happy to say that I feel like these six, in their different facets - as guides, as companions, as confidants - will forever be with me, and that we, sooner or later, will see each other again. ¡Saludos amigos! Les quiero mucho.
