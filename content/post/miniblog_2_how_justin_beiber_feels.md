{
 "disqus_url" : "http://conr.ca/post/miniblog_2_how_justin_beiber_feels/",
 "disqus_title" : "Miniblog 2: How Justin Beiber feels",
 "Title": "Miniblog 2: How Justin Beiber feels",
 "date": "2012-08-29",
 "Keywords": ["justin beiber", "tarapoto", "miniblog", "positionality"],
 "Tags": ["justin beiber", "tarapoto", "miniblog", "positionality"],
 "Slug": "miniblog_2_how_justin_beiber_feels",
 "Section": "post"
}
Yesterday afternoon we had a visit to the treatment plant by a group of third-year high school students. The tour went off pretty well - my colleague Roxana gave all the information and I just stood there doing my best not to look like a mute tourist. After the tour the students had questions to ask of us. One question was not an easy one, so Roxana's answer earned her a unanimous "In summary?" I figured I'd field this one for her - really the first I spoke to the group. My answer somehow merited applause. I am by no means ignorant of my positionality in Peru (You like that, IDSers?) - I am tall and white, if a little too skinny for the taste of most people here - I get a nice amount of looks, but by no means does the information I have to give merit applause. After the questions were done, we gave out pamphlets on the water we drink. Eventually, a girl asked me for a second pamphlet. I didn't really know why anyone would want a duplicate, but I hoped that she would share it with her folks - you know, get the message out there. In a few seconds I had given about nine or ten duplicates out and found myself surrounded by a gaggle of sixteen year old girls. I was asked everything from my name, to how long I've been in Tarapoto, to when I planned on leaving. When asked my age, I replied "23, a little bit old" one of my new found fans said, "23? I die!" (not entirely sure whether that is a good thing or not). I awkwardly counted and re-counted the pamphlets in my hand while my eyes and accent were discussed in giggles. Looking for an escape, I wandered over to Roxana. The group dispersed, but not before one girl yelled, "Bye Conor! I'll come back tomorrow!" I think I know what Justin Beiber feels like.
